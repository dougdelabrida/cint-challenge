# CI&T Front-End Challenge
Data provided by Marvel. © 2014 Marvel

This repository contains a simple web application named Marvel's © Comics App.
What does it allow you to do?

  - View a comics list and filter it.
  - View comic's details, such as, its authors, characters and serie.

### How to use it?

You can access it directly right [here][App] or run on your computer, by following steps.

Certify you have installed on your computer:
* [Node JS] - For development and dependecies management
* [Angular CLI] - Fantastic Angular generator!
* [Git] - 😃

Once having installed them, go to the installation step.

### Installation
This project was generated with [Angular CLI] version 1.0.6.

Open your favorite Terminal and run these commands.

In case you don't have Angular CLI installed.
`npm install -g @angular/cli`

Clone this repo into new project folder
`git clone https://github.com/dougdelabrida/cint-challenge`  
`cd cint-challenge`

Finally install the dependencies.
`npm install`.

### Setting your Marvel © API KEY
Access [Developer Marvel] to get one API KEY

Edit `src/environments/environment.js` replacing <YOUR PUBLIC API KEY> with your API KEY:
````
export const environment = {
  production: false,
  marvel: {
    endpoint: 'https://gateway.marvel.com:443/v1/public/',
    apikey: '<YOUR PUBLIC API KEY>'
  }
};
````

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Todos

 - Write Tests
 - Implement Authors Component

License
----

MIT


   [dill]: <https://github.com/dougdelabrida/dillinger>
   [app]: <https://cint-challenge-doug.surge.sh/>
   [Node JS]: <http://nodejs.org>
   [Angular]: <http://angular.io>
   [Angular CLi]: <https://github.com/angular/angular-cli>
   [Git]: <https://github.com/git/git>
   [Developer Marvel]: <https://developer.marvel.com>
