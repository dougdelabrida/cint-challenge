import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComicsListComponent } from "app/comics/list/list.component";

const routes: Routes = [
  {
    path: '',
    component: ComicsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
