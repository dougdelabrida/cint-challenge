import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ComicsService } from "../comics.service";
import { Router, ActivatedRoute } from "@angular/router";

class Period {
  name: string;
  value: string;
}

@Component({
  selector: 'comic-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ComicsListComponent implements OnInit, OnDestroy, AfterViewInit {

  isFetchingData: boolean = false;
  comics: Array<Object> = [];
  scrollListener: any = null;
  openedComic: number = 0;

  private comicsSub: any;
  private routeSub: any;

  private lastScrollTop = 0;

  periods: any = {
    selected: 'thisWeek',
    available: [
      {
        name: 'This Week',
        value: 'thisWeek'
      },
      {
        name: 'Last Week',
        value: 'lastWeek'
      },
      {
        name: 'Next Week',
        value: 'nextWeek'
      },
      {
        name: 'This month',
        value: 'thisMonth'
      }
    ]
  }

  constructor(
    private comicsService: ComicsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getList(false);
  }

  ngOnDestroy(): void {
    if (this.comicsSub) this.comicsSub.unsubscribe();
  }

  ngAfterViewInit() {
    const el = document.querySelector('#comics-wrapper');
    this.scrollListener = document.addEventListener('scroll', () => {
      const scrollingDown = document.body.scrollTop > this.lastScrollTop;
      if ((document.body.scrollTop + (document.body.scrollTop * 2 / 3) >= el.scrollHeight) && scrollingDown) {
        this.lastScrollTop = document.body.scrollTop;
        this.getList(true);
      }
    })
  }

  private getList(concat: boolean): void {

    const cWidth = document.body.clientWidth;
    let limit = 5;

    if (cWidth > 960) limit = 16;

    if (cWidth < 720 && cWidth > 500) limit = 12;    

    if (this.isFetchingData) return;

    if (!concat) this.comics = [];

    this.isFetchingData = true;

    const queries = [
      {'limit': limit},
      {'offset': concat ? this.comics.length : 0},
      {'orderBy': 'focDate'},
      {'dateDescriptor': this.periods.selected}
    ];

    this.comicsSub = this.comicsService.getComics(queries).subscribe(({data}) => {
      this.isFetchingData = false;
      this.comics = concat ? [...this.comics, ...data.results] : data.results;
    });
  }

  loadMore = this.getList;

  changePeriod(): void {
    this.lastScrollTop = 0;
    this.getList(false);
  }

  openComic(id): void {
    document.querySelector('body').style.overflow = 'hidden';
    this.openedComic = id;
  }

  closeComic(): void {
    document.querySelector('body').style.overflow = 'auto';
    this.openedComic = 0;
  }
}
