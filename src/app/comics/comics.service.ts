import { Injectable, EventEmitter } from '@angular/core';
import { environment } from 'environments/environment';
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ComicsService {

  constructor(private http: Http) { }

  private comicsUrl = 'comics';

  getComics(queries: Array<Object>) : Observable<any> {

    let plainQueries: string = '';
    
    queries.forEach((query, i) => Object.keys(query).forEach(key => {
      plainQueries += `${i === 0 ? '?' : '' }${key}=${query[key]}&`
    }))

    return this.http
                .get(`${environment.marvel.endpoint}${this.comicsUrl}${plainQueries}apikey=${environment.marvel.apikey}`)
                .map((res: any) => res.json())
                .catch((error: any) => Observable.throw(error.json().error || 'Unexpected error.'))
  }

}
