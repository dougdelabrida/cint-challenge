import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MdToolbarModule, MdSelectModule, MdCardModule, MdProgressSpinnerModule, MdIconModule, MdButtonModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import { ComicsListComponent } from './comics/list/list.component';
import { LayoutComponent } from './shared/layout/layout.component';
import { ComicComponent } from './comics/comic/comic.component';
import { ComicsService } from "./comics/comics.service";

@NgModule({
  declarations: [
    AppComponent,
    ComicsListComponent,
    LayoutComponent,
    ComicComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    MdToolbarModule,
    MdSelectModule,
    MdCardModule,
    MdProgressSpinnerModule,
    MdIconModule,
    MdButtonModule,
    BrowserAnimationsModule
  ],
  providers: [ComicsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
