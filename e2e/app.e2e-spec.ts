import { CintChallengePage } from './app.po';

describe('cint-challenge App', () => {
  let page: CintChallengePage;

  beforeEach(() => {
    page = new CintChallengePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
